<?php

class Event {

	protected $name;
	public $data;
	
	public function __construct($name, $data = null) {
	
		$this->name = $name;
		$this->data = $data;
	
	}
	
	public function getName() {
		return $this->name;
	}

}