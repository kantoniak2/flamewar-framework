<?php

class EventDispatcher {

	protected static $instance;
	protected $handlers = array();
	
	private function __construct() {}
	
	public static function instance() {
	
		if (!self::$instance) {
			self::$instance = new EventDispatcher();
		}
		
		return self::$instance;
	
	}
	
	public static function addEventListener($eventName, $eventHandler) {
	
		if (!is_callable($eventHandler) && !($eventHandler instanceof Closure))
			throw new Exception('Event handler is not callable neither anonymous function.');
			
		$dispatcher = EventDispatcher::instance();
		$dispatcher->handlers[$eventName][] = $eventHandler;
	
	}
	
	public function dispatchEvent(Event $event) {
	
		foreach ($this->handlers[$event->getName()] as $handler) {
			if (is_callable($handler))
				call_user_func($handler, $event);
			else
				$handler($event);
		}
	
	}

}