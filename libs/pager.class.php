<?php

class Pager {

	protected $pages;
	protected $currentPage;
	
	public function __construct($pages, $currentPage = 1) {
	
		$this->pages = $pages;
		$this->currentPage = $currentPage;
	
	}
	
	public function hasPrevious() {
		return $this->currentPage > 1;
	}
	
	public function getPrevious() {
		return $this->currentPage - 1;
	}
	
	public function hasNext() {
		return $this->currentPage < $this->pages;
	}
	
	public function getNext() {
		return $this->currentPage + 1;
	}
	
	public function getPages() {
		return $this->pages;
	}
	
	public function getCurrentPage() {
		return $this->currentPage;
	}

}