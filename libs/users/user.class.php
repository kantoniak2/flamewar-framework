<?php

class User {

	protected $id;
	protected $username;
	protected $accessLevel;
	protected $loggedIn;

	public function User($id = 0, $username = null, $accessLevel = 0, $loggedIn = false) {

		$this->id = $id;
		$this->username = $username;
		$this->accessLevel = $accessLevel;
		$this->loggedIn = $loggedIn;

	}

	public function isLoggedIn() {
		return $this->loggedIn;
	}

	public function getID() {
		return $this->id;
	}

	public function getUsername() {
		return $this->username;
	}
	
	public function getAccessLevel() {
		return $this->accessLevel;
	}

	public function __toString() {
		return "User #{$this->id}/{$this->username}/{$this->accessLevel}/".($this->loggedIn ? "zalogowany" : "niezalogowany");
	}

}