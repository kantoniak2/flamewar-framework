<?php

class UserAuth {

	protected $pdo;
	protected $user;
	
	public function __construct($autologin = true) {
	
		$this->pdo = PDOSingleton::instance();
		$userData = $_SESSION['user'];

		if ($autologin && is_array($userData))
			$this->user = new User($userData['id'], $userData['name'], $userData['accessLevel'], true);
		else
			$this->user = new User();
	
	}
	
	public function logIn($username, $password) {
	
		if ($this->user->isLoggedIn())
			return true;
	
		$stmt = $this->pdo->prepare("SELECT id, username, access_level FROM users WHERE username=:username AND password=:password");
		$stmt->bindValue(':username', $username, PDO::PARAM_STR);
		$stmt->bindValue(':password', hash('sha256', $password), PDO::PARAM_STR);
		$stmt->execute();

		if ($stmt->rowCount() !== 1)	
			return false;

		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		$stmt->closeCursor();
		
		$this->user = new User($row['id'], $row['username'], $row['access_level'], true);
		
		$_SESSION['user'] = array(
			'id' => $this->user->getID(),
			'name' => $this->user->getUsername(),
			'accessLevel' => $this->user->getAccessLevel()
		);
		
		return true;
	
	}
	
	public function getUser() {
		return $this->user;
	}

	public function logOut() {
		$this->user = new User;
		unset($_SESSION['user']);
	}
	
}