-- Adminer 3.3.4 MySQL dump

SET NAMES utf8;
SET foreign_key_checks = 0;
SET time_zone = 'SYSTEM';
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

CREATE TABLE `photos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `author_id` int(11) NOT NULL,
  `datetime` datetime NOT NULL,
  `title` varchar(200) NOT NULL,
  `location` varchar(200) NOT NULL,
  `desc` text NOT NULL,
  `image_url` varchar(300) NOT NULL,
  `image_license` varchar(30) DEFAULT NULL,
  `image_author` varchar(30) DEFAULT NULL,
  `image_authorlink` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `photos` (`id`, `author_id`, `datetime`, `title`, `location`, `desc`, `image_url`, `image_license`, `image_author`, `image_authorlink`) VALUES
(1,	1,	'2012-04-12 19:54:51',	'Mauna Kea',	'New York, New York, ASU',	'Jeden z największych wulkanów na Ziemii. W języku hawajskim nazwa brzmi \"biała góra\" - jest bardzo trafna, gdyż ma 4207m wysokości, przez co cały rok jest pokryty śniegiem. Na szczycie wulkanu znajduje się obserwatorium astronomiczne używane przez astrologów na całym świecie. Wulkan jest raczej nieaktywny, ostatnia erupcaj miała miejsce 3500 lat temu, a wciągu 10 000 lat wybuchła raptem kilka razy.',	'http://amleather.pl/photoalbum/images/mauna_kea.jpg',	'CC BY-NC-ND 2.0',	'Nataraj Met',	'http://www.flickr.com/photos/timrawle/3543949926/'),
(2,	1,	'2012-04-25 09:22:27',	'Nowojorska oaza zieleni',	'New York, New York, ASUS',	'Central Park leży w centrum Manhattanu. Jest dosyć stary, zaczęto nad nim pracować w 1857 roku. Budowany przez 15 lat, wymagał pracy 20 tysięcy osób. Stworzony w stylu angielskim, słynie z dużej ilości drzew, które całkowicie zasłaniają zabudowania miasta. To największe w mieście centrum wypoczynkowe, znajduje się tam między innymi zoo. Jest też pomnik króla Władysława Jagiełły, sławiący jego wygraną w bitwie pod Grunwaldem w 1410 roku. każdego roku park odwiedza 20 mld ludzi.',	'http://amleather.pl/photoalbum/images/nyc.jpg',	'CC BY-NC-SA 2.0',	'Nataraj Met',	'http://www.flickr.com/photos/natarajam/4002657625/'),
(3,	1,	'2012-04-25 09:45:35',	'Rynek krakowski',	'New York, New York, ASU',	'Rynek Główny w Krakowie to najważniejsza przestrzeń miejska byłej stolicy. Jest miejscem o wyjątkowym znaczeniu kulturowym i historycznym. Na rynku znajduje się wiele zabytków, takich jak Kościół Mariacki, Sukiennice, Kościół św. Wojciecha czy Wieża Ratuszowa. Codziennie, każdej godziny, na pamiątkę najazdu Tatarów w 1251 roku jest odgrywany hejnał mariacki, który nagle się urywa. Charakterystyczną melodię kojarzą nie tylko mieszkańcy Krakowa, a wszyscy Polacy, a w szczególności słuchacze Programu pierwszego Polskiego Radia, które transmituje wykonanie na żywo każdego dnia o godzinie 12.',	'http://amleather.pl/photoalbum/images/rynek_krakowski.jpg',	'CC BY-NC-ND 2.0',	'Nataraj Met',	'http://www.flickr.com/photos/pawelbak/'),
(4,	1,	'2012-04-26 21:32:48',	'Elektrownia w Czarnobylu',	'New York, New York, ASU',	'W elektrowni atomowej w Czarnobylu miała miejsce największa katastrofa atomowa ubiegłego wieku - jeden z reaktorów uległ awarii i eksplodował. Zawinili ludzie obsługujący maszynerię - eksperymentalnie wyłączono układ chłodzenia w czasie testów, które powinny być wykonane jeszcze przed oddaniem obiektu do eksploatacji. Radioaktywna chmura skaziła okolicę na wiele lat, a pierwiastki rozniosły się po całej Europie. Na zdjęciu opuszczony dom kultury miasteczka Prypeć w okolicy obiektu.',	'http://amleather.pl/photoalbum/images/czarnobyl.jpg',	'CC BY-SA 2.0',	'Nataraj Met',	NULL),
(5,	1,	'2012-04-26 21:34:36',	'Góry Anaga',	'New York, New York, ASU',	'Masyw Anaga znajduje się w północno-wschodniej części wyspy. Góry u wybrzeża są zdominowane przez skały, przez co nie ma tam zbyt wielu plaż dla turystów. Brak miejsc rekreacji rekompensują zachwycające widoki z jednego ze szczytów, który osiąga 1024 m n.p.m. U podnóża masywu leży stolica wyspy - Santa Cruz de Tenerife. Teneryfa jest jedną z popularnych lokacji wakacyjnych.',	'http://amleather.pl/photoalbum/images/anaga.jpg',	'CC BY-NC-SA 2.0',	'Nataraj Met',	'http://www.flickr.com/photos/pano_philou/'),
(6,	1,	'2012-04-26 21:43:44',	'Grobowce w Petrze',	'New York, New York, ASU',	'Petra to ruiny miasteczka Nabatejczyków, starożytnego ludu z Półwyspu Arabskiego, w południowo-zachodniej Jordanii. Rozciąga się wzdłuż drogi prowadzonej wąskim wąwozem - w jego skałach wykuto budowle miasta. Można tam znaleźć między innymi grobowce i pałace władców oraz teatr mieszczący nawet 10000 widzów. Historia miasta siędzy czasów ludzi pierwotnych, kiedy schronienia szukali tam pierwotni myśliwi.',	'http://amleather.pl/photoalbum/images/petra.jpg',	NULL,	'Nataraj Met',	NULL);

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(32) NOT NULL,
  `password` char(64) NOT NULL,
  `access_level` tinyint(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `users` (`id`, `username`, `password`, `access_level`) VALUES
(1,	'Kikert',	'9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08',	1);

-- 2012-06-17 17:58:04