<?php

session_start();

// Essential paths
define('STARTED', 1);
define('CMD', 'cmd/');
define('VIEWS', 'views/');
define('PLUGINS', 'plugins/');
define('LIBS', 'libs/');
define('CORE', 'core/');

// Dynamic autoload
$includeFolders = Array(CORE, CMD, LIBS);
if ($handle = opendir(realpath(LIBS))) {
    while (false !== ($entry = readdir($handle)))
		if ($entry !== '.' && $entry !== '..' && is_dir(LIBS.$entry))
			$includeFolders[] = LIBS.$entry.'/';
    closedir($handle);
}

// Define autoload
spl_autoload_extensions('.class.php, .php');
set_include_path(get_include_path(). PATH_SEPARATOR .implode(PATH_SEPARATOR, $includeFolders));
spl_autoload_register();

Debugger::setErrorReporting(Config::$debug);

$logger = Logger::instance();
$logger->logMessage('Uruchomienie FW', LOGGER_DEBUG, 'Core');

PluginsManager::instance()->loadAllPlugins(PLUGINS);
EventDispatcher::instance()->dispatchEvent(new Event('onAfterPlugins'));

$mainRequest = new Request();
$controller = new Controller($mainRequest);
EventDispatcher::instance()->dispatchEvent(new Event('onBeforeDispatch', array($mainRequest)));
$controller->dispatch();