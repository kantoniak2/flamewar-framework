<?php

abstract class Command {

	protected $controller;
	protected $view;
	protected $template;
	
	protected $defaultAction;
	
	final public function __construct(Controller &$controller) {
	
		$this->controller = $controller;
		$this->view = new View();

	}
	
	public function display() {
		
		if (!$this->template)
			throw new RuntimeException("Nie podano szablonu do wykonania");
	
		$this->set('commandName', $this->controller->getCommandName());
		$this->set('actionName', $this->controller->getActionName());
		return $this->view->fetch($this->template);
		
	}
    
	public function beforeExecute() {}
	public function afterExecute() {}
	
	protected function set($name, $value) {
		$this->view->set($name, $value);
	}
	
	protected function get($name) {
		return $this->view->get($name);
	}
	
	public function getDefaultAction() {
		return $this->defaultAction;
	}
	
}
