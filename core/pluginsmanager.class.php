<?php

class PluginsManager {

	protected static $instance;
	protected $loadedPlugins = array();

	private function __construct() {}
	
	public static function instance() {
	
		if (!self::$instance)
			self::$instance = new PluginsManager();
		
		return self::$instance;
	
	}

	public function loadPlugins(Array $paths) {
	
		foreach ($paths as $plugin) {
			if (include $plugin)
				$this->loadedPlugins[] = $plugin;
		}
	
	}

	public function loadAllPlugins($dir) {
	
		foreach (glob("{$dir}*.php") as $plugin) {
			if (include $plugin)
				$this->loadedPlugins[] = $plugin;
		}
	
	}

}