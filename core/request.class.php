<?php

/**
 * Klasa do obsługi danych wejściowych żądania.
 * Dane są pobierane z $_SERVER['PATH_INFO']
 * Czyli URL wygląda np. tak: http://localhost/fw/index.php/login/user=test/
 *  => $request::params == Array('login', 'user' => 'test');
 * 
 */
class Request {

	protected $cmdName;
	protected $actionName;
	protected static $translations;
	public static $params;

	public function __construct($autoRoute = TRUE) {
		if ($autoRoute) {
			$path = substr($_SERVER['QUERY_STRING'], 1);
			$this->route($path);
		}
	}
	
	public function route($query) {
	
		if (!$query) {
			$this->cmdName = Config::$default_command;
		} else {
		
			// Rozdziel slashe
			$query = preg_replace('/\/$/', '', $query);
			$params = explode('/', $query);
			
			// Przypisanie akcji z aliasami
			$this->cmdName =	lcfirst(join(array_map('ucfirst', explode('-', self::translateAlias(array_shift($params))))));
			$this->actionName = lcfirst(join(array_map('ucfirst', explode('-', self::translateAlias(array_shift($params))))));
			
			self::$params = Array();
			$keys = Array();
			
			// Przypisz tylko bez kluczy
			foreach($params as $p) {
				$arr = explode('=', $p);
				if(count($arr) == 1)
					self::$params[] = $arr[0];
				else
					$keys[$arr[0]] = $arr[1];
			}
			
			// Dodaj wartości z kluczami
			foreach($keys as $key => $value)
				self::$params[$key] = $value;
		}
	
	}
	
	protected static function translateAlias($in, $invert = false) {
	
		if (!self::$translations) {
			@ include_once 'translations.php';
			self::$translations = $translations;
		}
	
		if (!$invert)
			return (self::$translations[$in] ? self::$translations[$in] : $in);
		else
			foreach (self::$translations as $key => $rule)
				if ($in == $rule)
					return $key;
					
		return $in;
		
	}
	
	public function getCmdName() {
		return $this->cmdName;
	}
	
	public function getActionName() {
		return $this->actionName;
	}
	
	static function createURL(Array $elements = array()) {
		
		$url = Config::$base_address;

		if (count($elements))
			$url .= (Config::$mod_rewrite ? '' : 'index.php/');
	
		$isAction = 2;
		foreach($elements as $key => $value) {
			if (!$value)
				continue;
			if(is_numeric($key)) {
				if ($isAction--)
					$url .= self::translateAlias($value, true).'/';
				else
					$url .= $value.'/';
			} else
				$url .= $key.'='.$value.'/';
		}
		
		return $url;
	}

}
