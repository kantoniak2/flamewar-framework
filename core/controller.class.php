<?php

class Controller {

	protected $cmdName;
	protected $actionName;
	protected $cmd;

	public function __construct(Request $request) {

		$this->cmdName = $request->getCmdName();
		$this->actionName = $request->getActionName();

	}

	public function dispatch() {

		try {

			$className = ucfirst($this->cmdName) .'Command';

			if (!include_once CMD. strtolower($this->cmdName) .'command.class.php')
				throw new RuntimeException("Nie udało się załadować pliku obsługi żądania: {$className} dla wywołania {$_SERVER['QUERY_STRING']}");

			$this->cmd = new $className($this);

			if (!$this->actionName && $this->cmd->getDefaultAction())
				$this->actionName = $this->cmd->getDefaultAction();

			if (!method_exists($this->cmd, $this->actionName))
				throw new RuntimeException("Podana akcja nie jest obsługiwana przez komendę lub brak domyślnej akcji ({$this->actionName})");

			$this->cmd->beforeExecute();
			$this->cmd->{$this->actionName}();
			$this->cmd->afterExecute();

			echo $this->cmd->display();

		} catch (Exception $e) {

			Logger::instance()->logMessage("Błąd wykonywania żądania: {$e->getMessage()}", LOGGER_ERROR, 'Core');

			$view = new View();
			$view->set('exc', $e);
			echo $view->fetch('e404');

		}

	}

	public function getCommandName() {
		return $this->cmdName;
	}

	public function getActionName() {
		return $this->actionName;
	}

}
