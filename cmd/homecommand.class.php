<?php

class HomeCommand extends Command {

	protected $defaultAction = 'execute';

	public function execute() {
	
		try {
			
			Debugger::debug(
				'Kikert, test',
				hash('sha256', 'test')
			);
			
			Debugger::debug(
				'Przykładowy URL',
				Request::createURL(array('contact', 'lubie-placki'))
			);

		} catch (Exception $e) {
			throw $e;
		}
	
		$this->set('log', $log);
		$this->template = 'home';
	
	}

}