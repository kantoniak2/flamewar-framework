<?php

class UserCommand extends Command {

	protected $defaultAction = 'login';

	public function afterExecute() {
		$this->template = 'home';
	}

	public function login() {
		
		try {
			$ua = new UserAuth();
			if (!$ua->login('Kikert', 'test'))
				throw new Exception('Podano nieprawidłowe dane!');
		} catch (Exception $e) {
			Debugger::debug('lastE', $e->getMessage());
		}
		
	}

	public function logout() {
		try {
			$ua = new UserAuth();
			$ua->logOut();
		} catch (Exception $e) {
			Debugger::debug('lastE', $e->getMessage());
		}
	}
	
}

?>
