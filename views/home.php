<?php require_once VIEWS.'inc/top.php'; ?>

<h2>Strona startowa Flamewar Framework</h2>
<p>Flamewar Framework to lekka aplikacja do szybkiego tworzenia nowych stron, stworzona z naciskiem na szybką rozbudowę i przejrzystość kodu.</p>

<h2>Wersja 1.0.5</h2>
<ul>
	<li>Request przyjmuje adresy z akcjami typu "do-something" przekształcając je na akcje doSomething()</li>
	<li>Menedżer wtyczek, odpytywanie wszystkich wtyczek przed uruchomieniem systemu (z <code>PLUGINS</code>)</li>
	<li>Klasa Pager</li>
	<li>Klasa Event przekazuje zdarzenia z parametrem</li>
</ul>

<h2>Minimalna instalacja</h2>
<ul>
	<li>Kopia pliku <code>index.php</code></li>
	<li>Kopia folderów <code>CORE</code>, <code>LIBS</code></li>
	<li>Puste foldery <code>CMD</code>, <code>VIEWS</code></li>
	<li>Komenda z jedną akcją i jeden widok</li>
	<li>Widok <code>e404</code></li>
	<li>Uzupełniony plik <code>config.class.php</code></li>
</ul>

<?php require_once VIEWS.'inc/footer.php'; ?>