<?php require_once VIEWS.'inc/top.php'; ?>

<?php

$photos = $this->get('photosCollection');
//Debugger::debug('col', $collection);

$table = new PhotosTable($photos);

$table->translateKeys(array(
	'id' => '#',
	'author_username' => 'Autor',
	'datetime' => 'Data dodania',
	'title' => 'Tytuł',
	'location' => 'Lokacja',
));

$table->selectColumns(array('author_username', 'desc', 'image_url', 'image_license', 'image_author', 'image_authorlink'));
echo $table->render();

?>

<?php require_once VIEWS.'inc/footer.php'; ?>