<!DOCTYPE HTML>
<html>
<head>
	<meta charset="utf-8" />
	<title>Flamewar Framework</title>
	<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,700' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="<?php echo Request::createURL(); ?>media/css/debug.css" />
<style>

* { margin: 0; padding: 0; }
html { height: 100%; }
body { display: table !important; width: 100%; height: 100%; background: url(<?php echo Request::createURL(); ?>/media/images/bg.jpg); }
body > div { display: table-cell; width: 100%; height: 100%; vertical-align: middle; }
body > div > div { width: 900px; margin: 50px auto; background: #FFFFFF; border-radius: 10px; box-shadow: 0px 0px 15px rgba(0, 0, 0, 0.8); }

nav { display: block; height: 50px; background: #31BA4B; border-bottom: 16px solid #3A3D46; }
nav ul { display: block; float: right; overflow: hidden; padding-right: 16px; }
nav li { display: block; float: left; }
nav a { display: block; padding: 0 16px; font: 15px 'Source Sans Pro', sans-serif; line-height: 50px; color: #3A3D46; text-transform: uppercase; text-decoration: none; }
nav a:hover { color: #EEE; background: #207A31; background: #ED7B56; background: #227433; }
section { padding: 25px 50px 45px; }

h1, h2, h3 { font: 22px 'Source Sans Pro', sans-serif; color: #44302D; }
h1 { display: block; padding-left: 82px; line-height: 100px; font-size: 34px; font-weight: normal; color: #EEE !important; background: #3A3D46 url(<?php echo Request::createURL(); ?>/media/images/logo.png) 32px center no-repeat; border-radius: 4px 4px 0 0; }
h2 { margin: 20px 0 10px; }

p, li, a { font: 14px Tahoma; color: #6A6766; line-height: 150%; }
p { margin: 10px 0; }
ul { padding-left: 18px; }

#debug_data > table { margin: 20px 0 0 !important; }

footer { height: 60px; border-top: 1px solid #CFCCCA; background: #3A3D46; border-radius: 0 0 4px 4px; }
footer > p { font: bold 11px 'Arial'; color: #CCC; line-height: 40px; text-align: center; text-transform: uppercase; }

</style>

</head>
<body>

<div>
<div>

<h1>Flamewar Framework</h1>

<nav>
	<ul>
		<li><a href="<?php echo Request::createURL(); ?>">Strona główna</a></li>
		<li><a href="<?php echo Request::createURL(array('user', 'login')); ?>">Zaloguj</a></li>
		<li><a href="<?php echo Request::createURL(array('user', 'logout',)); ?>">Wyloguj</a></li>
	</ul>
</nav>

<section>

<?php Debugger::debug('$_SESSION', $_SESSION); ?>
